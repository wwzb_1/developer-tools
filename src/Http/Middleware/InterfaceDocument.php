<?php

namespace Cy\DeveloperUtil\Http\Middleware;

use Closure;
use Cy\DeveloperUtil\Http\Traits\InterfaceDocumentationTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class InterfaceDocument
{
    use InterfaceDocumentationTrait;

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!$this->getInterFaceTitle()) return $next($request);

        $data = $this->sqlData();

        Cache::put($data['url'] . '_' . $data['method'], $data, now()->addMinutes(5));

        return $next($request);
    }
}
