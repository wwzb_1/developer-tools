<?php

namespace Cy\DeveloperUtil\Http\Providers;

use Cy\DeveloperUtil\FastDfs\FastBaseAdapter;
use Cy\DeveloperUtil\FastDfs\FastDfsAdapter;
use Cy\DeveloperUtil\FastDfs\FastFilesystem;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\ServiceProvider;

class FastDfsProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Storage::extend('fastdfs', fn($app, $config) => new FastBaseAdapter(new FastFilesystem(new FastDfsAdapter($config))));
    }
}
