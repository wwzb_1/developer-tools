<?php

namespace Cy\DeveloperUtil\Http\Providers;

use Cy\DeveloperUtil\Http\Service\JWTCyService;
use Illuminate\Auth\GuardHelpers;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

class JwtGuard implements Guard
{
    use GuardHelpers;

    public const PREFIX = 'jwt_';

    /**
     * @var Request
     */
    private Request $request;
    private string $name;

    /**
     * Create a new authentication guard.
     *
     * @param Request $request
     * @param string $name
     */
    public function __construct(Request $request, string $name)
    {
        $this->request = $request;
        $this->name = $name;
    }

    /**
     * Get the currently authenticated user.
     *
     * @return mixed
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function user()
    {
        // If we've already retrieved the user for the current request we can just
        // return it back immediately. We do not want to fetch the user data on
        // every call to this method because that would be tremendously slow.
        if (!is_null($this->user)) {
            return $this->user;
        }

        $user = null;

        $token = $this->getTokenForRequest();

        # 解析token
        if (!empty($token)) {
            $uuid = $this->validate(['api_token' => $token])['sub'] ?? null;
            $user = $this->checkUser($uuid);
        }

        return $this->user = $user;
    }

    /**
     * 获取用户信息
     * @param $uuid
     * @param int $exp
     * @param array $user
     * @return mixed
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    protected function checkUser($uuid, $exp = 0, $user = [])
    {
        $prefix = self::PREFIX . $uuid;
        # 根据uuid查询redis中是否存有用户信息
        if (empty($user)) {
            if (cache()->has($prefix)) return cache()->get($prefix);
            $model = config('auth.guards.' . $this->name . '.provider');
            $model = config('auth.providers.' . $model . '.model');
            $user = $model::firstWhere(['uuid' => $uuid, 'status' => '启用']);
        }

        if ($user) {
            cache()->put($prefix, $uuid, $exp - time() - 60);
            return $user;
        }

        abort(4013, '账号不存在，或被禁用');
    }

    /**
     * 获取请求头中的bearerToken
     *
     * @return string
     */
    public function getTokenForRequest(): string
    {
        return $this->request->bearerToken() ?? '';
    }

    /**
     * 解析token
     *
     * @param array $credentials
     * @return array
     */
    public function validate(array $credentials = []): array
    {
        return JWTCyService::verifyToken($credentials['api_token']);
    }

    /**
     * Set the current request instance.
     *
     * @param \Illuminate\Http\Request $request
     * @return $this
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;

        return $this;
    }
}
