<?php

namespace Cy\DeveloperUtil\Http\Providers;

use App\Http\Service\BaseService;
use Cy\DeveloperUtil\Http\Traits\InterfaceDocumentationTrait;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class ResponseMacroServiceProvider extends ServiceProvider
{
    use InterfaceDocumentationTrait;

    /**
     * 通用响应格式
     */
    protected array $resString = [
        'code' => 'code',
        'message' => 'message',
        'content' => 'content',
        'contentEncrypt' => 'contentEncrypt',
    ];

    /**
     * 响应码
     */
    private $code = 0;

    /**
     * 响应消息
     */
    private string $message = '';

    /**
     * 响应内容
     */
    private $content = '';

    /**
     * 加密内容
     */
    private $contentEncrypt = '';

    /**
     * 注册响应宏
     *
     * @return void
     * @throws FileNotFoundException
     */
    public function boot()
    {
        $_this = $this;

        Response::macro('success', function ($data = '', $message = '', $contentEncrypt = [], $code = 0) use ($_this) {

            $_this->setCode($code);
            $_this->setContent($data);
            $_this->setMessage($message);
            $_this->setContentEncrypt($contentEncrypt);

            # 只有正常响应的时候，再进行文档输出
            if (!$code) {
                # 进行接口文档输出判断
                $res = $_this->interfaceDocument();

                if (is_string($res)) return response($res)->withHeaders(['Content-Type' => 'json; charset=UTF-8']);
            } else {
                # 异常code直接返回
                $res = $_this->getRes();
            }

            return new JsonResponse($res, 200);
        });
    }

    /**
     * 输出接口文档
     * @return mixed
     * @throws FileNotFoundException
     *
     */
    public function interfaceDocument()
    {
        if ($this->getInterFaceTitle()) {

            $content = $this->getDocumentParams();

            $res = $this->getRes();

            if (is_array($res['content']) && array_key_exists('skip', $res['content']) && array_key_exists('limit', $res['content'])) {
                $content['params'] = isset($content['params']) ? json_decode($content['params']) : [];
                $content['params'] = [...BaseService::defaultListParams(), ...$content['params']];
            }

            $content['response'] = json_encode($res);

            # 接口文档入库
            $content = $this->saveInterface($content);

            return $this->replaceContent($content->toArray());
        }

        return $this->getRes();
    }


    /**
     * 设置响应码
     * @param $code
     * @return ResponseMacroServiceProvider
     */
    public function setCode($code): ResponseMacroServiceProvider
    {
        $this->code = $code;
        return $this;
    }

    /**
     * 设置响应消息
     * @param String $message
     * @return ResponseMacroServiceProvider
     */
    public function setMessage(string $message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * 设置响应内容
     * @param $content
     * @return ResponseMacroServiceProvider
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * 设置响应加密内容
     * @param $contentEncrypt
     * @return ResponseMacroServiceProvider
     */
    public function setContentEncrypt($contentEncrypt)
    {
        if (!empty($contentEncrypt)) $this->contentEncrypt = Hash::make(json_encode($contentEncrypt));
        return $this;
    }

    /**
     * 生成返回数据
     */
    public function getRes(): array
    {
        $res = [
            $this->resString['code'] => $this->code,
            $this->resString['message'] => $this->message,
            $this->resString['content'] => $this->content,
            $this->resString['contentEncrypt'] => $this->contentEncrypt,
        ];

        info('【REQUEST】' . $this->getUrl() . PHP_EOL, [
            'params' => request()->input(),
            'response' => $res
        ]);

        return $res;
    }
}
