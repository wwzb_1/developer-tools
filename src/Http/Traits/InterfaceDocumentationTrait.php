<?php

namespace Cy\DeveloperUtil\Http\Traits;

use Cy\DeveloperUtil\Models\InterfaceDocument;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Validation\Concerns\FormatsMessages;
use Illuminate\Validation\ValidationRuleParser;

trait InterfaceDocumentationTrait
{
    use FormatsMessages;

    public $fallbackMessages = [];
    public $customMessages = [];
    public $validator = null;

    /**
     * The size related validation rules.
     *
     * @var string[]
     */
    protected array $sizeRules = ['Size', 'Between', 'Min', 'Max', 'Gt', 'Lt', 'Gte', 'Lte'];

    /**
     * The numeric related validation rules.
     *
     * @var string[]
     */
    protected $numericRules = ['Numeric', 'Integer'];

    /**
     * Get the stub file for the generator. 获取生成器模板文件
     *
     * @return string
     */
    protected function getStub()
    {
        $stub = '/stubs/Interface.documentation.plain.stub';
        return file_exists($customPath = base_path(trim($stub, '/')))
            ? $customPath
            : __DIR__ . '/../../Commands/'.$stub;
    }

    /**
     * 替换内容
     *
     * @param null $content
     * @return string|string[]
     * @throws FileNotFoundException
     */
    protected function replaceContent($content = null)
    {
        $files = new Filesystem();
        # 读取文件内容
        $stub = $files->get($this->getStub());

        # 获取接口信息
        $content = $content ?? $this->getDocumentParams();

        $param = '`无`';

        if (!empty($content['params'])) {

            $temp = is_array($content['params']) ? $content['params'] : json_decode($content['params'], true);

            $param = "\n\r";
            $param .= "| 参数名 | 参数类型 | 示例参数 | 必填项 | 名称 | 注释 | \n";
            $param .= "| --- | --- | --- | --- | --- | --- | \n";

            foreach ($temp as $key => $item) {

                if (is_object($item['example']) || is_array($item['example'])) {
                    $item['example'] = json_encode($item['example']);
                }

                $param .= '| ' . $item['name'];
                $param .= ' | ' . $item['type'];
                $param .= ' | ' . $item['example'];
                $param .= ' | ' . $item['is_empty'];
                $param .= ' | ' . $item['describe'];
                $param .= ' | ' . ($item['remark'] ?? '');
                $param .= ' | ' . "\n";
            }
        }

        # 如果为GET请求
        return str_replace(
            ['TITLE', 'URL', 'ROUTE', 'METHOD', 'CONTENT', '`无`'],
            [$content['title'], $content['url'], $content['as'], $content['method'], $content['response'], $param],
            $stub
        );
    }

    /**
     * 获取标题并判标题
     * @return mixed
     */
    protected function getInterFaceTitle()
    {
        $title = request()->filled('interfaceTitle') ? request()->interfaceTitle : false;

        if ($title) {

            $index = strrpos($title, '*');

            if ($index === false) {
                $title = '### ' . $title;
            } else {
                $title = substr_replace($title, "* ", $index, 1);
                $title = str_replace("*", "#", $title);
            }
        }

        return $title;
    }

    /**
     * 处理【表单验证类】接口参数
     * @param $validator
     */
    protected function setDocumentParams($validator)
    {
        if (!$this->getInterFaceTitle()) return;

        $data = $this->sqlData($this->setParams($validator));

        # 存入缓存
        Cache::put($data['url'] . '_' . $data['method'], $data, now()->addMinutes(5));
    }

    /**
     * 获取URi
     */
    public function getInterFaceUrl()
    {
        return request()->route()->uri;
    }

    /**
     * 获取URl
     */
    protected function getUrl()
    {
        return request()->path();
    }

    /**
     * 获取路由别名
     */
    public function getInterFaceRoute()
    {
        return request()->route()->getName();
    }

    /**
     * 获取请求方法
     */
    public function getInterFaceMethod()
    {
        return request()->getMethod();
    }

    public $data = [];
    /**
     * The validation rules that imply the field is required.
     *
     * @var string[]
     */
    protected array $implicitRules = [
        'Accepted',
        'Filled',
        'Present',
        'RequiredIf',
        'RequiredUnless',
        'RequiredWith',
        'RequiredWithAll',
        'RequiredWithout',
        'RequiredWithoutAll',
    ];

    /**
     * 接口参数
     * @param $param
     * @param string $type
     * @return mixed
     */
    public function setParams($param, $type = 'validator'): array
    {
        $this->validator = $param;
        $this->data = $param->getData();
        $this->translator = $param->getTranslator();
        $params = [];
        $all = [];

        if ($type == 'validator') {

            foreach ($param->getRules() as $key => $item) {

                # 是否必填
                $params['name'] = $key;
                $params['is_empty'] = '×';
                $params['type'] = 'other';
                $params['example'] = $param->getData()[$key] ?? '';
                $params['describe'] = $param->customAttributes[$key] ?? Arr::get($this->translator->get('validation.attributes'), $key);
                $params['remark'] = '';

                foreach ($item as $i) {

                    [$rule, $parameters] = ValidationRuleParser::parse($i);

                    if ($rule == 'Required') {
                        $params['is_empty'] = '√';
                    }

                    if (in_array($rule, $this->implicitRules)) {
                        $params['is_empty'] = '※';
                        $params['remark'] .= ',' . $this->makeReplacements(
                                $this->getMessage($key, $rule), $key, $rule, $parameters
                            );
                    }

                    if (in_array($rule, $this->sizeRules)) {
                        $params['remark'] .= ',' . $this->makeReplacements(
                                $this->getMessage($key, $rule), $key, $rule, $parameters
                            );
                    }

                    $params['remark'] = trim($params['remark'], ',');

                    switch ($i) {
                        case 'array':
                        case 'string':
                        case 'date':
                        case 'file':
                        case 'uuid':
                            $params['type'] = $i;
                            break;
                        case 'integer':
                            $params['type'] = 'int';
                            break;
                        case 'numeric':
                            $params['type'] = 'float';
                            break;
                        case 'boolean':
                            $params['type'] = 'boolean';
                            break;
                    }
                }

                array_push($all, $params);
            }
        }

        return $all;
    }


    /**
     * 接口文档入库
     * @param array $content
     * @return mixed
     */
    protected function saveInterface($content = [])
    {
        $content = empty($content) ? $this->getDocumentParams() : $content;
        if (is_array($content['params'] ?? '')) $content['params'] = json_encode($content['params']);

        return InterfaceDocument::updateOrCreate([
            'url' => $this->getInterFaceUrl(),
            'method' => $this->getInterFaceMethod()
        ], $content);
    }

    /**
     * 获取缓存中的接口数据
     */
    protected function getDocumentParams()
    {
        $data = $this->sqlData();

        return Cache::get($data['url'] . '_' . $data['method']) ?? $data;
    }

    /**
     * 入库参数
     * @param null $params
     * @return array
     */
    public function sqlData($params = null): array
    {
        $data = [
            'title' => $this->getInterFaceTitle(),
            'url' => $this->getInterFaceUrl(),
            'as' => $this->getInterFaceRoute(),
            'method' => $this->getInterFaceMethod(),
            'response' => null,
        ];

        if ($params) $data['params'] = json_encode($params);

        return $data;
    }

    protected $translator;

    protected function getValue($attribute)
    {
        return Arr::get($this->data, $attribute);
    }

    protected $implicitAttributes = [];

    protected function getPrimaryAttribute($attribute)
    {
        foreach ($this->implicitAttributes as $unparsed => $parsed) {
            if (in_array($attribute, $parsed, true)) {
                return $unparsed;
            }
        }

        return $attribute;
    }

    /**
     * Determine if the given attribute has a rule in the given set.
     *
     * @param string $attribute
     * @param string|array $rules
     * @return bool
     */
    public function hasRule($attribute, $rules)
    {
        return $this->validator->hasRule($attribute, $rules);
    }

    /**
     * Get a rule and its parameters for a given attribute.
     *
     * @param string $attribute
     * @param string|array $rules
     * @return array|null
     */
    protected function getRule($attribute, $rules)
    {
        return $this->validator->getRule($attribute, $rules);
    }
}
