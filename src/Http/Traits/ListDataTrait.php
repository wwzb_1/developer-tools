<?php

namespace Cy\DeveloperUtil\Http\Traits;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

trait ListDataTrait
{
    /**
     * 处理where条件
     * @param $data
     * @param $tableName
     * @return array
     */
    protected static function whereFilled($data, $tableName): array
    {
        $where = [];
        $orWhere = [];
        $all = [];

        foreach ($data as $key => $item) {
            $field = '';

            # 判断参数类型
            if (is_string($item)) {

                # 字符串类型：直接进行where = 查询
                $field = $item;
                if (request()->filled($field)) $where[$field] = request()->input($field);

            } elseif (is_array($item)) {

                # 数组类型：分为kw查询和条件where查询
                $field = $item[0];

                if ($field == 'kw') {

                    # kw 查询
                    if (request()->filled('kw')) {

                        # 支持多条件
                        $kw_name = explode(',', $item[1]);


                        if (count($kw_name) > 1) {
                            $orWhere[0][] = [$kw_name[0], 'like', '%' . request()->input('kw') . '%'];

                            for ($i = 1; $i < count($kw_name); ++$i) {

                                $orWhere[1][] = [$kw_name[$i], 'like', '%' . request()->input('kw') . '%'];
                            }
                        } else {
                            $where[] = [$kw_name[0], 'like', '%' . request()->input('kw') . '%'];
                        }
                    }
                } else {
                    # 其它查询
                    if (request()->filled($field)) $where[] = $item;
                }
            } else {
                abort(5000, 'whereFilled只支持字符串与数组');
            }

            if (request()->filled('interfaceTitle') && $tableName) {

                $sql = "SHOW FULL COLUMNS FROM `{$tableName}`";
                $result = DB::select($sql);

                foreach ($result as $i) {

                    if ($i->Field == $field) {

                        if (strstr($i->Type, 'enum')) {
                            $params['type'] = 'string';
                        } elseif (strstr($i->Type, 'int')) {
                            $params['type'] = 'integer';
                        } elseif (strstr($i->Type, 'varchar')) {
                            $params['type'] = 'string';
                        } elseif ($field == 'kw') {
                            $params['type'] = 'string';
                        } elseif ($field == 'image') {
                            $params['type'] = 'image';
                        } elseif ($field == 'file') {
                            $params['type'] = 'file';
                        } else {
                            $params['type'] = 'other';
                        }

                        $params['name'] = $i->Field;
                        $params['is_empty'] = '×';
                        $params['example'] = request()->input($field) ?? '';
                        $params['describe'] = $i->Comment;

                        array_push($all, $params);
                    }
                }

                if ($field == 'kw') {
                    $params['type'] = 'string';
                    $params['name'] = 'kw';
                    $params['is_empty'] = '×';
                    $params['example'] = request()->input($field) ?? '';
                    $params['describe'] = '模糊搜索，以' . $item[1] . '字段进行搜索';

                    array_push($all, $params);
                }
            }
        }

        $url = request()->route()->uri;
        $method = request()->getMethod();
        $data = Cache::get($url . '_' . $method);
        $data['params'] = json_encode($all);
        Cache::put($url . '_' . $method, $data, 180);

        return ['where' => $where, 'orWhere' => $orWhere];
    }

    /**
     * 使用datatable，获取列表页
     * @param array $condition 列表页查询条件
     * @param null $obj 查询对象
     * @return array
     */
    public static function getLists(array $condition = [], $obj = null): array
    {
        # 给查询条件设定初始值
        $where = $condition['where'] ?? false;
        $whereBetween = $condition['whereBetween'] ?? false;
        $select = $condition['select'] ?? ['*'];
        $whereIn = $condition['whereIn'] ?? false;
        $groupBy = $condition['groupBy'] ?? false;
        $orWhere = $condition['orWhere'] ?? false;
        $with = $condition['with'] ?? false;
        $withCount = $condition['withCount'] ?? false;
        $order_name = $condition['order_name'] ?? 'id';
        $order_type = $condition['order_type'] ?? 'desc';
        $limit = $condition['limit'] ?? 10;
        $skip = $condition['skip'] ?? 0;
        $resource = $condition['resource'] ?? false;
        $whereFilled = $condition['whereFilled'] ?? false;
        $whereHas = $condition['whereHas'] ?? false;
        $has = $condition['has'] ?? false;
        $whereHasMulti = $condition['whereHasMulti'] ?? false;

        # 获取排序字段
        $order_column = request()->input('order_name', $order_name);
        # 获取排序类型
        $order_type = request()->input('order_type', $order_type);
        # 接收是否获取所有数据
        $limit = request()->input('limit', $limit);

        if ($limit != -1) {
            # 接收分页数据
            $start = request()->input('skip', $skip);
        } else {
            $start = '';
            $limit = '全部';
        }

        $data = [
            'skip' => $start,
            'limit' => $limit,
        ];

        # 查询表
        if (!$obj) $obj = static::class;
        if (is_string($obj)) $obj = new $obj;

        # 判断是否有其它参数
        if ($whereFilled) $whereFilled = static::whereFilled($whereFilled, $obj->getModel()->getTable());

        $sql = $obj
            ->when($withCount, fn($q) => $q->withCount($withCount))
            ->when($with, fn($q) => $q->with($with))
            ->when($where, fn($q) => $q->where($where))
            ->when(!empty($whereFilled['where']), fn($q) => $q->where($whereFilled['where']))
            ->when(!empty($whereFilled['orWhere']), fn($q) => $q->where(fn($where) => $where->where($whereFilled['orWhere'][0])->orWhere($whereFilled['orWhere'][1])))
            ->when($orWhere, fn($q) => $q->where(fn($where) => $where->where($orWhere[0])->orWhere($orWhere[1])))
            ->when($whereBetween, fn($q) => $q->whereBetween($whereBetween[0], $whereBetween[1]))
            ->when($whereIn, fn($q) => $q->whereIn($whereIn[0], $whereIn[1]))
            ->when($whereHas, fn($q) => $q->whereHas($whereHas[0], $whereHas[1]))
            ->when($has, fn($q) => $q->has($has))
            ->when($groupBy, fn($q) => $q->groupBy($groupBy));

        if (!empty($whereHasMulti)) {
            foreach ($whereHasMulti as $one) {
                $sql = $sql->whereHas($one[0], $one[1]);
            }
        }

        # 计算总数
        $data['count'] = $sql->count();

        # 表格数据
        $data['data'] = $sql
            # 获取分页数据，如果为-1获取所有
            ->when($limit !== '全部', fn($q) => $q->offset($start)->limit($limit))
            # 获取排序数据
            ->orderBy($order_column, $order_type)
            # 默认以ID降序排列
//            ->latest('id')
            # 获取集合实例
            ->get($select);

        if ($resource) {
            if ($resource === true) {
                $resource = basename(str_replace('\\', '/', get_called_class()));
                $resource = 'App\\Http\\Resources\\' . $resource . '\\' . $resource . 'Resource';
            }

            $data['data'] = $resource::collection($data['data']);
        }

        # 处理数据
        return $data;
    }
}
