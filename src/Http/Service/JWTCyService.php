<?php

namespace Cy\DeveloperUtil\Http\Service;

use App\Http\Service\BaseService;
use App\Models\Client\Manager;
use DateInterval;
use DateTimeInterface;
use Exception;
use Firebase\JWT\JWT;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class JWTCyService extends BaseService
{
    # 缓存前缀
    public const PREFIX = 'jwt_';

    # 过期时间
    public static ?DateTimeInterface $tokensExpireAt;

    # key
    protected $encrypter;

    public function __construct()
    {
        static::$tokensExpireAt ??= Carbon::now()->addDay();
        $this->encrypter = app('encrypter');
    }

    /**
     * 发放token
     * @param $uuid
     * @return string
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @throws Exception
     */
    protected function createToken($uuid): string
    {
        cache()->delete(self::PREFIX . $uuid);
        # 当前时间
        $time = time();

        $token = [
            # 签发时间
            'iat' => $time,
            # (Not Before)：某个时间点后才能访问，比如设置time+30，表示当前时间30秒后才能使用
            'nbf' => $time,
            # 过期时间
            'exp' => static::$tokensExpireAt->getTimestamp(),
            # 携带的用户信息
            'sub' => $uuid
        ];

        # 签发token
        return JWT::encode($token, $this->encrypter->getKey());
    }

    /**
     * 设置token过期时间
     *
     * @param \DateTimeInterface|null $date
     * @return mixed
     */
    protected function tokensExpireIn(DateTimeInterface $date = null)
    {
        if (is_null($date)) {
            return static::$tokensExpireAt
                ? Carbon::now()->diff(static::$tokensExpireAt)
                : new DateInterval('P1Y');
        }

        static::$tokensExpireAt = $date;

        return new static;
    }

    /**
     * 解析token
     * @param $token
     * @return mixed
     */
    protected function verifyToken($token): array
    {
        try {
            JWT::$leeway = 60;//当前时间减去60，把时间留点余地
            # HS256方式，这里要和签发的时候对应
            return (array)JWT::decode($token, $this->encrypter->getKey(), ['HS256']);
        } catch (\Firebase\JWT\SignatureInvalidException $e) {  //签名不正确
            abort(401, $e->getMessage());
        } catch (\Firebase\JWT\BeforeValidException $e) {  // 签名在某个时间点之后才能用
            abort(401, $e->getMessage());
        } catch (\Firebase\JWT\ExpiredException $e) {  // token过期
            abort(401, $e->getMessage());
        } catch (Exception $e) {  //其他错误
            abort(401, $e->getMessage());
        }
    }

    /**
     * 重新生成并返回token
     * @return bool|string
     */
    public function refreshToken($key)
    {
        if (empty($key)) {
            return $this->responseInvalidParams('key is empty');
        }

        $newKey = encrypt(decrypt($key));//重新生成key
        $time = time(); //当前时间
        $overtime = 7200;
        $token = [
            'iat' => $time, //签发时间
            'nbf' => $time, //(Not Before)：某个时间点后才能访问，比如设置time+30，表示当前时间30秒后才能使用
            'exp' => $time + $overtime, //过期时间,这里设置2个小时
        ];
        if (!$token = JWT::encode($token, $newKey, 'HS256')) {
            \Log::error('app api operation failed：key=' . $newKey . ', token=' . $token);
            return response()->json(['error' => 1, 'msg' => 'operation failed', 'data' => []]);
        }

        return response()->json(['error' => 0, 'msg' => 'ok', 'data' => ['key' => $key, 'token' => $token, 'expire' => $overtime]]);
    }
}
