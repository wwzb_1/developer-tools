<?php

namespace Cy\DeveloperUtil\Http\Service;

use App\Http\Service\BaseService;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class PortalService extends BaseService
{
    /**
     * 燕山门户接口地址
     * @var mixed
     */
    private $host;

    /**
     * AppKey
     * @var mixed
     */
    private $key;

    /**
     * AppSecret
     * @var mixed
     */
    private $secret;

    public function __construct()
    {
        $this->host = rtrim(env('YS_PLATFORM_HOST'), '/');
        $this->key = env('YS_PLATFORM_ID');
        $this->secret = env('YS_PLATFORM_SECRET');
    }

    /**
     * 获取燕山门户的access_token
     */
    protected function ys_access_token()
    {
        # 1.判断缓存中是否有token
        if (Cache::has('ys_access_token')) return Cache::get('ys_access_token');

        # 2.缓存中如果没有token，则调用方法获取
        $token = $this->oauthToken();

        # 3.将token存入缓存，并设置过期时间
        Cache::put('ys_access_token', $token['access_token'], $token['expires_in']);

        # 4.返回token
        return $token['access_token'];
    }

    # 证书上报接口
    protected function certificateReport($data)
    {
        $url = $this->host . '/api/open/certificates';
        return $this->res($url, $data);
    }

    # 开始更新证书
    protected function certificateUpdate($subject)
    {
        $url = $this->host . '/api/open/certificates/' . $subject;
        return $this->res($url, null, 'put');
    }

    # 新增管理员接口
    protected function managerStore($data)
    {
        $url = $this->host . '/api/open/managers';
        return $this->res($url, $data);
    }

    # 密码登录接口
    protected function secretLogin($data)
    {
        $url = $this->host . '/api/open/login/secrets';
        return $this->res($url, $data);
    }

    # 修改密码接口
    protected function passwordEdit($data)
    {
        $url = $this->host . '/api/open/passwords';
        return $this->res($url, $data, 'put');
    }

    # 调用token接口
    private function oauthToken()
    {
        $url = $this->host . '/oauth/token';
        return Http::post($url, [
            'token_name' => env('APP_NAME'),
            'grant_type' => 'client_credentials',
            'client_id' => $this->key,
            'client_secret' => $this->secret,
            'scope' => '*',
        ])->json();
    }

    # 处理返回结果
    private function res($url, $data, $method = 'post')
    {
        $http = Http::withToken($this->ys_access_token())->withHeaders(['X-Requested-With' => 'XMLHttpRequest']);
        $data = $http->$method($url, $data)->json();

        if (!$data) abort(500, '【门户反馈】服务器错误');

        if ($data['code']) {
            if ($data['code'] == 401) {
                Cache::delete('ys_access_token');
                abort(1401, '【门户反馈】客户端授权已失效');
            }
            abort('1' . $data['code'], '【门户反馈】' . $data['message']);
        }

        return $data['content'] ? $data['content'] : $data['message'];
    }
}
