<?php

namespace Cy\DeveloperUtil;

use Cy\DeveloperUtil\Commands\ControllerCommand;
use Cy\DeveloperUtil\Commands\ServiceCommand;
use Cy\DeveloperUtil\Commands\UtilCommand;
use Cy\DeveloperUtil\Facades\DeveloperUtil;
use Cy\DeveloperUtil\Http\Middleware\InterfaceDocument;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class DeveloperUtilProvider extends ServiceProvider
{
    /**
     * 服务提供者加是否延迟加载.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * 启动应用服务
     *
     * @return void
     */
    public function boot()
    {
        # 发布迁移文件
        $this->loadMigrationsFrom(__DIR__ . '/Migrations/2020_12_18_174319_create_interface_documents_table.php');

        # 注册 Artisan 命令
        if ($this->app->runningInConsole()) {
            $this->commands([
                ServiceCommand::class,
                UtilCommand::class,
                ControllerCommand::class,
            ]);
        }

        # 发布BASE类
        $this->publishes([
            __DIR__ . '/Base/BaseController.php.stub' => app()->path('Http/Controllers/BaseController.php'),
            __DIR__ . '/Base/BaseService.php.stub' => app()->path('Http/Service/BaseService.php'),
            __DIR__ . '/Base/BaseModel.php.stub' => app()->path('Models/BaseModel.php'),
            __DIR__ . '/Base/BaseRequest.php.stub' => app()->path('Http/Requests/BaseRequest.php'),
            __DIR__ . '/Base/documentation.stub' => '接口文档.md',
        ], 'base');

        # 发布模版文件
        $this->publishes([
            __DIR__ . '/Commands/stubs' => 'stubs',
        ], 'stubs');

        # 注册接口文档中间件
        $this->app['router']->prependMiddlewareToGroup('api', InterfaceDocument::class);

        # 注册接口文档路由及视图
        $this->registerRoutes();

        $this->loadViewsFrom(
            __DIR__ . '/resources/views', 'documentation'
        );
    }

    /**
     * 注册路由
     *
     * @return void
     */
    private function registerRoutes()
    {
        Route::group($this->routeConfiguration(), function () {
            $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
        });
    }

    /**
     * Get the Telescope route group configuration array.
     *
     * @return array
     */
    private function routeConfiguration(): array
    {
        return [
            'namespace' => 'Cy\DeveloperUtil\Http\Controllers',
            'prefix' => 'cy/documentation',
        ];
    }

    /**
     * 注册服务提供者。
     *
     * @return void
     */
    public function register()
    {
        # 单例绑定服务
        $this->app->singleton('DeveloperUtil', function ($app) {
            return new DeveloperUtil();
        });
    }

    /**
     * 获取由提供者提供的服务。
     *
     * @return array
     */
    public function provides(): array
    {
        return [DeveloperUtil::class];
    }
} 
