<?php

namespace Cy\DeveloperUtil\Facades;

use Illuminate\Support\Facades\Facade;

class DeveloperUtil extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'DeveloperUtil';
    }
}
