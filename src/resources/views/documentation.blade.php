<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="https://yandex.st/highlightjs/8.0/styles/solarized_dark.min.css">
    <script src="https://yandex.st/highlightjs/8.0/highlight.min.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>
    {{--    <script src="https://cdn.jsdelivr.net/npm/clipboard@2.0.6/dist/clipboard.min.js"></script>--}}
    <title>接口文档</title>
    <style>
        p{
            margin: 12px;
        }

        hr{
            margin: 16px auto;
        }

        html, body {
            background-color: #fff;
            color: #222;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
            padding-bottom: 30px;
        }

        .app {
            display: flex;
            justify-content: space-between;
        }

        .container {
            width: 70%;
            margin-right: 20px;
            padding: 20px;
        }

        table {
            width: 100%;
        }

        table,
        table tr th,
        table tr td {
            border: 1px solid #ede;
            margin: 0;
            padding: 10px 10px;
            /* 合并边框 */
            border-collapse: collapse;
        }

        pre {
            background: #F8F8F8;
            color: #0a0302;
            padding: 20px 10px;
            overflow: auto;
            border: 1px solid #ede;
        }

        p code {
            color: #1f6fb2;
            cursor: pointer;
        }

        li code {
            font-size: 28px;
            color: #4eb4ee;
            font-weight: bold;
        }

        #catalog {
            position: fixed;
            top: 0;
            padding-top: 30px;
            width: 20%;
            height: 95%;
            overflow-y: scroll;
            left: 16px;
        }


        #catalog *{
            padding: 0;
            margin: 0;
        }

        .nothing {
            width: 10px;
            height: 10px;
        }

        a {
            text-decoration: none;
            line-height: 30px;
        }

        .one > a {
            font-weight: bold;
        }

        #catalog h1 {
            margin: 10px 0;
            border-bottom: 1px dashed #1c3d5a;
        }

        #catalog h1 > a {
            font-size: 20px;
        }

        #catalog h2 > a, h3 > a, h4 > a, h5 > a {
            font-size: 14px;
            line-height: 16px;
        }

        #catalog h1 > a::before {
            content: '※ '
        }

        #catalog  h1 > a::after {
            content: ' ※'
        }

        #catalog h2 > a {
            padding-left: 10px;
        }

        #catalog h3 > a {
            padding-left: 20px;
        }

        #catalog  h3 > a {
            font-weight: normal;
            padding-left: 30px;
        }

        #catalog  h4 > a {
            font-weight: normal;
            padding-left: 40px;
        }

        #input{
            margin-top: 200px;
        }


    </style>
</head>
<body>
<div id="catalog">
</div>
<div class="app">
    <div class="nothing"></div>
    <div class="container">
        {!! $content !!}
    </div>
</div>
<input id="input" type="text" value="" readonly>
</body>
<script>
    var click_cnt = 0;
    var $html = document.getElementsByTagName("html")[0];
    var $body = document.getElementsByTagName("body")[0];

    $("code[class!='language-json']").on("click", function (event) {
        let text = event.target.innerText;
        let input = document.getElementById("input");
        input.value = text; // 修改文本框的内容
        input.select(); // 选中文本
        if (document.execCommand("copy")) {
            let $elem = document.createElement("b");
            $elem.style.color = "#E94F06";
            $elem.style.zIndex = 9999;
            $elem.style.position = "absolute";
            $elem.style.select = "none";
            let x = event.pageX;
            let y = event.pageY;
            $elem.style.left = (x - 10) + "px";
            $elem.style.top = (y - 20) + "px";
            clearInterval(anim);
            $elem.innerText = "❤ 复制成功";
            $elem.style.fontSize = Math.random() * 10 + 8 + "px";
            var increase = 0;
            var anim;
            setTimeout(function () {
                anim = setInterval(function () {
                    if (++increase == 150) {
                        clearInterval(anim);
                        $body.removeChild($elem);
                    }
                    $elem.style.top = y - 20 - increase + "px";
                    $elem.style.opacity = (150 - increase) / 120;
                }, 8);
            }, 70);
            $body.appendChild($elem);
        }
    });

    function AddLi(i, index) {
        let oLi = document.createElement(i.tagName);
        let a = document.createElement("a");
        oLi.append(a);
        a.href = `#${i.innerText}-${index}`;
        a.innerHTML = i.innerText;
        return oLi
    }

    let hs = document.querySelectorAll("h1, h2, h3, h4");

    hs.forEach( (item, index) => {
        item.id = `${item.innerText}-${index}`;
        let oLi = AddLi(item, index);
        document.querySelector("#catalog").append(oLi);
    })
</script>
</html>
