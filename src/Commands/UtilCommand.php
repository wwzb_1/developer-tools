<?php

namespace Cy\DeveloperUtil\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class UtilCommand extends Command
{
    /**
     * 命令名称及签名
     *
     * @var string
     */
    protected $signature = 'make:util {component} {--set=CSMQRT} {--controller=Backend} {--explorer=} {--route=}';

    /**
     * 命令描述
     *
     * @var string
     */
    protected $description = '创建开发模块';

    /**
     * 创建命令
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 执行命令
     *
     * @param Artisan $drip
     * @return mixed
     */
    public function handle(Artisan $drip)
    {
        $name = $this->getNameInput();
        $option = $this->getOptionInput();
        $controller = $this->getControllerInput();
        $route = $this->getRouteInput();
        $explorer = empty($this->getExplorerInput()) ? $name : $this->getExplorerInput();

        for ($i = 0; $i < strlen($option); $i++) {
            // CSMQRT
            switch ($option[$i]) {
                case 'c':
                    $drip::call('make:util-controller', [
//                        'name' => $controller . '/' . $explorer . '/' . $name . 'Controller',
                        'name' => $controller . '/' . $name . 'Controller',
                        '--model' => $name,
                        '--explorer' => $explorer,
                        '--route' => $route,
                    ]);
                    echo '控制器创建成功' . PHP_EOL;
                    break;
                case 's':
                    $drip::call('make:service', [
                        'service'=> $explorer . '/' . $name,
                        '--explorer' => $explorer,
                    ]);
                    echo '服务层创建成功' . PHP_EOL;
                    break;
                case 'm':
                    $m = strpos($option, 't') !== false ? '-m' : '';
                    $drip::call('make:model ' .str_replace('\\', '/', $explorer) . '/' . $name . ' ' . $m);
                    echo '模型创建成功' . PHP_EOL;
                    break;
                case 'q':
                    $drip::call('make:request ' . str_replace('\\', '/', $explorer) . '/' . $name . 'Request');
                    echo '请求类创建成功' . PHP_EOL;
                    break;
                case 'r':
                    $drip::call('make:resource ' . str_replace('\\', '/', $explorer) . '/' . $name . 'Resource');
                    echo '资源响应类创建成功' . PHP_EOL;
                    break;
            }
        }
    }

    /**
     * Get the desired class name from the input. 从输入中获取所需的类名
     *
     * @return string
     */
    protected function getNameInput(): string
    {
        return trim($this->argument('component'));
    }

    /**
     * Get the desired class name from the input. 从输入中获取所需的类名
     *
     * @return string
     */
    protected function getOptionInput(): string
    {
        return strtolower(trim($this->option('set')));
    }

    /**
     * Get the desired class name from the input. 从输入中获取所需的类名
     *
     * @return string
     */
    protected function getControllerInput(): string
    {
        return trim($this->option('controller'));
    }

    /**
     * Get the desired class name from the input. 从输入中获取所需的类名
     *
     * @return string
     */
    protected function getRouteInput(): string
    {
        return trim($this->option('route'));
    }

    /**
     * Get the desired class name from the input. 从输入中获取所需的文件夹名
     *
     * @return string
     */
    protected function getExplorerInput(): string
    {
        return trim($this->option('explorer'));
    }
}
