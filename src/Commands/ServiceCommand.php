<?php

namespace Cy\DeveloperUtil\Commands;

use Illuminate\Console\Command;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;

class ServiceCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:service {service} {--inherit=} {--explorer=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '创建service类';
    /**
     * @var Filesystem
     */
    private $files;
    private $type = '';

    /**
     * Create a new command instance.
     *
     * @param Filesystem $files
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }


    /**
     * Execute the console command. 执行控制台命令
     * @return bool
     * @throws FileNotFoundException
     */
    public function handle()
    {
        $name = $this->qualifyClass($this->getNameInput());
        $path = $this->getPath($name);

        if ((!$this->hasOption('force') ||
                !$this->option('force')) &&
            $this->alreadyExists($this->getNameInput())) {
            $this->error($this->type . ' already exists!');

            return false;
        }

        // Next, we will generate the path to the location where this class' file should get
        // written. Then, we will build the class and make the proper replacements on the
        // stub files so that it gets the correctly formatted namespace and class name.
        $this->makeDirectory($path);

        $this->files->put($path, $this->buildClass($name));

        $this->info($this->type . ' created successfully.');
    }

    /**
     * Get the stub file for the generator. 获取生成器模板文件
     *
     * @return string
     */
    protected function getStub()
    {
        $stub = $stub ?? '/stubs/services.plain.stub';

        return $this->resolveStubPath($stub);
    }

    /**
     * Resolve the fully-qualified path to the stub.
     *
     * @param  string  $stub
     * @return string
     */
    protected function resolveStubPath($stub)
    {
        return file_exists($customPath = $this->laravel->basePath(trim($stub, '/')))
            ? $customPath
            : __DIR__.$stub;
    }

    /**
     * Get the default namespace for the class. 获取该类的默认名称空间
     *
     * @return string
     */
    protected function getDefaultNamespace()
    {
        return '\Http\Service';
    }

    /**
     * * Build the class with the given name. 使用给定的名称构建类
     *
     * Remove the base controller import if we are already in base namespace. 如果我们已经在基命名空间中，则删除基控制器导入
     *
     * @param $name
     * @return mixed
     * @throws FileNotFoundException
     */
    protected function buildClass($name)
    {
        return $this->buildClassParent($name);
    }

    /**
     * Build the class with the given name. 使用给定的名称构建类
     *
     * @param $name
     * @return mixed
     * @throws FileNotFoundException
     */
    protected function buildClassParent($name)
    {
        $name = 'App' . $name;
        $stub = $this->files->get($this->getStub());

        return $this->replaceNamespace($stub, $name)->replaceClass($stub, $name)
            ->replaceInheritClass($stub, $this->option('inherit'));
    }

    /**
     * Parse the class name and format according to the root namespace.  根据根命名空间解析类名和格式
     *
     * @param string $name
     * @return string
     */
    protected function qualifyClass($name)
    {
        $name = ltrim($name, '\\/');
        $rootNamespace = $this->rootNamespace();

        if (Str::startsWith($name, $rootNamespace)) {
            return $name;
        }

        $name = str_replace('/', '\\', $name);

        return $this->getDefaultNamespace() . '\\' . $name;
    }

    /**
     * Determine if the class already exists.  确定类是否已经存在
     *
     * @param string $rawName
     * @return bool
     */
    protected function alreadyExists($rawName)
    {
        return $this->files->exists($this->getPath($this->qualifyClass($rawName)));
    }

    /**
     * Get the destination class path. 获取目标类路径
     *
     * @param string $name
     * @return string
     */
    protected function getPath($name)
    {
        return $this->laravel['path'] . '/' . str_replace('\\', '/', $name) . 'Service.php';
    }

    /**
     * Build the directory for the class if necessary. 如果需要，为类构建目录
     *
     * @param string $path
     * @return string
     */
    protected function makeDirectory($path)
    {
        if (!$this->files->isDirectory(dirname($path))) {
            $this->files->makeDirectory(dirname($path), 0777, true, true);
        }

        return $path;
    }

    /**
     * Replace the namespace for the given stub. Replace the namespace for the given stub
     *
     * @param string $stub
     * @param string $name
     * @return $this
     */
    protected function replaceNamespace(&$stub, $name)
    {
        $stub = str_replace(
            ['DummyNamespace', 'DummyRootNamespace', '{{ explorer }}'],
            [$this->getNamespace($name), $this->rootNamespace(), trim($this->option('explorer'))],
            $stub
        );
        return $this;
    }

    /**
     * Get the full namespace for a given class, without the class name. 获取给定类的完整名称空间，但不包含类名
     *
     * @param string $name
     * @return string
     */
    protected function getNamespace($name)
    {
        return trim(implode('\\', array_slice(explode('\\', $name), 0, -1)), '\\');
    }

    /**
     * Replace the class name for the given stub. 替换给定存根的类名
     *
     * @param string $stub
     * @param string $name
     * @return string
     */
    protected function replaceClass(&$stub, $name)
    {
        $class = str_replace($this->getNamespace($name) . '\\', '', $name);
        $stub = str_replace('DummyClass', $class, $stub);

        return $this;
    }

    /**
     * 自定义继承 引用
     * @param $stub
     * @param $Inherit
     * @return mixed
     */
    protected function replaceInheritClass($stub, $Inherit)
    {
        $class = str_replace($this->getNamespace($Inherit) . '\\', '', $Inherit);

        $use = count(explode('\\', $Inherit)) >= 2 ? "" : 'use App\Http\Service\\' . $Inherit;
        $stub = str_replace(['DummyUseNamespace', 'DummyInheritClass'], [$use, $class], $stub);

        return $stub;
    }

    /**
     * Get the desired class name from the input. 从输入中获取所需的类名
     *
     * @return string
     */
    protected function getNameInput()
    {
        return trim($this->argument('service'));
    }

    /**
     * Get the root namespace for the class. 获取类的根名称空间
     *
     * @return string
     */
    protected function rootNamespace()
    {
        return $this->laravel->getNamespace();
    }
}
