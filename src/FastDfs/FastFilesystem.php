<?php

namespace Cy\DeveloperUtil\FastDfs;

use InvalidArgumentException;
use League\Flysystem\Filesystem;
use League\Flysystem\Util;

class FastFilesystem extends Filesystem
{
    public function putStream($path, $resource, array $config = [])
    {
        if (!is_resource($resource) || get_resource_type($resource) !== 'stream') {
            throw new InvalidArgumentException(__METHOD__ . ' expects argument #2 to be a valid resource.');
        }

        $path = Util::normalizePath($path);
        $config = $this->prepareConfig($config);
        Util::rewindStream($resource);

        return $this->getAdapter()->writeStream($path, $resource, $config);
    }
}
