<?php

namespace Cy\DeveloperUtil\FastDfs;

use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Http\File;

class FastBaseAdapter extends FilesystemAdapter
{
    public function putFile($path, $file = null, $options = [])
    {
        if ($file === null) $file = $path;

        $file = is_string($file) ? new File($file) : $file;

        return $this->putFileAs($path, $file, $file->hashName(), $options);
    }

    public function putFileAs($path, $file, $name, $options = [])
    {
        $stream = fopen(is_string($file) ? $file : $file->getRealPath(), 'r');

        // Next, we will format the path of the file and store the file using a stream since
        // they provide better performance than alternatives. Once we write the file this
        // stream will get closed automatically by us so the developer doesn't have to.
        $result = $this->put(trim($path . '/' . $name, '/'), $stream, $options);

        if (is_resource($stream)) {
            fclose($stream);
        }

        return $result;
    }
}
