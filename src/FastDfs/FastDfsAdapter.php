<?php

namespace Cy\DeveloperUtil\FastDfs;

use finfo;
use League\Flysystem\Adapter\AbstractAdapter;
use League\Flysystem\Config;
use League\Flysystem\Util;
use League\Flysystem\Util\MimeType;

class FastDfsAdapter extends AbstractAdapter
{
    protected $pathSeparator = DIRECTORY_SEPARATOR;
    private Config $config;

    private $group = 'group1';

    private $server;

    public function __construct($config = null)
    {
        $this->setConfig($config);

        $server = fastdfs_connect_server($config['addr'], $config['port']);
        if (!$server) {
            error_log("fastdfs_connect_server errno: " . fastdfs_get_last_error_no() . ", error info: " . fastdfs_get_last_error_info());
            exit(1);
        }

        $this->server = $server;
        $this->setPathPrefix($this->group);
    }

    public function getUrl($path)
    {
        $config = $this->config;

        $path = $this->applyPathPrefix($path);

        if ($config->has('url')) {
            return $this->concatPathToUrl($config->get('url'), $path);
        }

        return $path;
    }

    public function write($path, $contents, Config $config)
    {
        # 从path中获取文件扩展名
        $extension = pathinfo($path, PATHINFO_EXTENSION);

        return fastdfs_storage_upload_by_filebuff1($contents, $extension);
    }

    public function writeStream($path, $resource, Config $config)
    {
        $local_filename = stream_get_meta_data($resource)['uri'];

        # 从path中获取文件扩展名
        $extension = pathinfo($path, PATHINFO_EXTENSION);

        $finfo = new Finfo(FILEINFO_MIME_TYPE);
        $mimetype = $finfo->file($local_filename);

        if (in_array($mimetype, ['application/octet-stream', 'inode/x-empty', 'application/x-empty'])) {
            $mimetype = MimeType::detectByFilename($local_filename);
        }

        $path = fastdfs_storage_upload_by_filename(
            $local_filename,
            $extension,
            ['mimetype' => $mimetype],
            $path ?: $this->group,
            fastdfs_tracker_get_connection(),
            fastdfs_tracker_query_storage_store()
        );

        return $path['filename'] ?? false;
    }

    public function putStream()
    {
        dd('putStream 不支持');
    }

    public function put($path, $contents, Config $config)
    {
        dd('put 不支持');
    }

    public function update($path, $contents, Config $config)
    {
        dd('update 不支持');
    }

    public function updateStream($path, $resource, Config $config)
    {
        dd('updateStream 不支持');

    }

    public function rename($path, $newpath)
    {
        dd('rename 不支持');
    }

    public function copy($path, $newpath)
    {
        $file = $this->read($path);
        return $this->write($newpath, $file['contents'], $this->config);
    }

    public function delete($path)
    {
        return fastdfs_storage_delete_file($this->group, $path);
    }

    public function deleteDir($dirname)
    {
        dd('deleteDir 不支持');
    }

    public function createDir($dirname, Config $config)
    {
        dd('createDir 不支持');
    }

    public function setVisibility($path, $visibility)
    {
        dd('setVisibility 不支持');
    }

    public function has($path)
    {
        return fastdfs_storage_file_exist($this->group, $path);
    }

    public function read($path)
    {
        $contents = fastdfs_storage_download_file_to_buff($this->group, $path);

        return ['type' => 'file', 'path' => $path, 'contents' => $contents];
    }

    public function readStream($path)
    {
        dd('readStream 不支持');
    }

    public function listContents($directory = '', $recursive = false)
    {
        dd('listContents 不支持');
    }

    public function getMetadata($path)
    {
        return fastdfs_storage_get_metadata($this->group, $path);
    }

    public function getSize($path)
    {
        $object = fastdfs_get_file_info($this->group, $path);
        $object['size'] = $object['file_size'];
        $object['timestamp'] = $object['create_timestamp'];
        return $object;
    }

    public function getMimetype($path)
    {
        $mimetype = $this->getMetadata($path);
        $mimetype['path'] = $path;
        $mimetype['type'] = 'file';

        return $mimetype;
    }

    public function getTimestamp($path)
    {
        return $this->getSize($path);
    }

    public function getVisibility($path)
    {
        dd('getVisibility 不支持');
    }

    protected function setConfig($config)
    {
        $this->config = $config ? Util::ensureConfig($config) : new Config;

        $this->group = $this->config->get('group', 'group1');
    }

    protected function concatPathToUrl($url, $path): string
    {
        return rtrim($url, '/') . '/' . ltrim($path, '/');
    }
}
