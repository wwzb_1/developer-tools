## 自定义文件系统-FASTDFS使用方法

## 1.驱动配置
​    laravel文件系统驱动配置信息位于 config/filesystems.php 配置文件中。该文件包含原框架驱动程序的示例配置数组。将fastdfs的相关信息写入该配置文件的disks数组中。

``` php
<?php

return [

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        's3' => [
            'driver' => 's3',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET'),
            'url' => env('AWS_URL'),
            'endpoint' => env('AWS_ENDPOINT'),
            'use_path_style_endpoint' => env('AWS_USE_PATH_STYLE_ENDPOINT', false),
        ],
        
    /*
    |--------------------------------------------------------------------------
    | FASTDFS 驱动配置
    |--------------------------------------------------------------------------
    |
    | 以下为配置信息示例，将以下内容复制到config/filesystems.php文件的disks数组中
    |
    */
        'fastdfs' => [
            'driver' => 'fastdfs',
            'url' => env('APP_URL'),
            'addr' => env('FASTDFS_IP', '172.18.132.233'),
            'port' => env('FASTDFS_PORT', 22122),
            'group' => env('FASTDFS_GROUP', 'group1'),
        ]
    ],

];
```

并将对应env变量写入

``` txt
# 文件系统默认使用的驱动
FILESYSTEM_DRIVER=fastdfs
# fastdfs客户端信息
FASTDFS_IP=172.18.132.233
FASTDFS_PORT=22122
FASTDFS_GROUP=group1
```

## 2.注册服务

打开 Laravel 的 config/app.php 文件，会看到 providers 数组。数组中的内容是应用程序要加载的所有服务提供者的类。将服务类写入到providers数组中。

``` php
'providers' => [
        /*
         * Laravel Framework Service Providers...
         */
    ...
    # 
    Cy\DeveloperUtil\Http\Providers\FastDfsProvider::class
];
```

## 3.可用的方法

``` php
	# 如果配置默认使用fastdfs，使用Storage时需要指定disk。
    # 创建一个文件
    $file_id = Storage::disk('fastdfs')->put('.txt', 'bbb'); # $file_id = 'M00/00/00/rBKE6WbkE6WABH_TAAAAA4hIY9I939.txt';

    # 将已有文件上传
    $file = '/data/ys_platform_php/storage/logs/laravel.log';

	# 如果在env中配置了FILESYSTEM_DRIVER=fastdfs，使用Storage时不需要指定disk
    $file_id = Storage::putFile(new File($file)); # $file_id = 'M00/00/00/rBKE6WcF_hSARC69AAABu8YDmZU968.log';

    $contents = Storage::disk('fastdfs')->url($file_id);
    $contents = Storage::disk('fastdfs')->get($file_id);
    $contents = Storage::disk('fastdfs')->exists($file_id);
    $contents = Storage::disk('fastdfs')->missing($file_id);
    $contents = Storage::disk('fastdfs')->download($file_id);
    $contents = Storage::disk('fastdfs')->size($file_id);
    $contents = Storage::disk('fastdfs')->lastModified($file_id);
    $contents = Storage::disk('fastdfs')->path($file_id);
    $contents = Storage::disk('fastdfs')->delete($file_id);
```

