<?php

use Illuminate\Support\Facades\Route;

// Mail entries...
Route::get('/interface/backend', function () {

    if (env('APP_ENV') == 'production') abort(404);

    $markdownParser = new Parsedown();
    $fileContent = request()->filled('home') ? file_get_contents(base_path('小程序接口文档.md')) : file_get_contents(base_path('接口文档.md'));
    $htmlContent = $markdownParser->setBreaksEnabled(true)->text($fileContent);
    $content = $markdownParser->setBreaksEnabled(true)->text($htmlContent);
    return view('documentation::documentation')->with('content', $content);

});
