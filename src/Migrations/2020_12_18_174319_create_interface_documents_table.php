<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInterfaceDocumentsTable extends Migration
{
    private const tableName = 'interface_documents';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = $this->down();

        Schema::create(static::tableName, function (Blueprint $table) {
            $table->id();

            $table->string('title')->default('')->comment('标题');
            $table->string('url')->default('')->comment('请求地址');
            $table->string('as')->default('')->comment('路由别名');
            $table->string('method')->default('')->comment('请求方法');
            $table->json('params')->comment('请求参数')->nullable();
            $table->json('response')->comment('响应示例')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });

        DB::statement("ALTER TABLE `" . env('DB_PREFIX', '') . static::tableName . "` comment '接口文档表'");

        if(!empty($data)){

            DB::table(static::tableName)->insert($data);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return array
     */
    public function down()
    {
        $data = [];
        # 判断表是否存在
        if (Schema::hasTable(static::tableName)) {
            # 表存在，则取出内容
            $data = DB::table(static::tableName)->whereNull('deleted_at')->get()->toArray();
        }

        Schema::dropIfExists(static::tableName);

        return $data;
    }
}
