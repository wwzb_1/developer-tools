## 安装命令

`composer require cy01/developer-utils`

## 发布配置

`php artisan vendor:publish --provider="Cy\DeveloperUtil\DeveloperUtilProvider"`

## 开发版本

> php 7.4
>
> laravel 8.28.1

## 新增命令行

`php artisan make:util {component} {--set=CSMQRT} {--controller=Backend} {--explorer=} {--route=}`

``` 
参数说明：
--set = C - 控制器， S - 服务类 ，M - 模型， Q - 验证类， R - 资源类，T - 迁移文件
--explorer  生成的文件夹名称  默认与模块名相同。
--route  对应的路由参数， 默认与模块名相同。
```